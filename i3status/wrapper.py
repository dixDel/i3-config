#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This script is a simple wrapper which prefixes each i3status line with custom
# information. It is a python reimplementation of:
# http://code.stapelberg.de/git/i3status/tree/contrib/wrapper.pl
#
# To use it, ensure your ~/.i3status.conf contains this line:
#     output_format = "i3bar"
# in the 'general' section.
# Then, in your ~/.i3/config, use:
#     status_command i3status | ~/i3status/contrib/wrapper.py
# In the 'bar' section.
#
# In its current version it will display the cpu frequency governor, but you
# are free to change it to display whatever you like, see the comment in the
# source code below.
#
# © 2012 Valentin Haenel <valentin.haenel@gmx.de>
#
# This program is free software. It comes without any warranty, to the extent
# permitted by applicable law. You can redistribute it and/or modify it under
# the terms of the Do What The Fuck You Want To Public License (WTFPL), Version
# 2, as published by Sam Hocevar. See http://sam.zoy.org/wtfpl/COPYING for more
# details.

import sys
import json
import platform
import subprocess
import os.path

def get_governor():
    """ Get the current governor for cpu0, assuming all CPUs use the same. """
    with open('/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor') as fp:
        return fp.readlines()[0].strip()

def get_linux_version():
    """docstring for get_linux_version"""
    rawList = platform.platform().split('-')
    return 'Linux ' + rawList[1] + ' ' + rawList[4]

scrollCharPosition = 0
spaces = ''
titlePrevious = ''
startDelay = 3
def get_cmus_status():
    """docstring for get_cmus_status"""
    cmus_status = ["", "", '\u23F8'] # note symbol
    isCmusRunning = 0
    isCmusPlaying = 0
    output = ''
    cmd = subprocess.Popen('cmus-remote -Q', shell=True, stdout=subprocess.PIPE)
    artist = ''
    albumartist = ''
    title = ''
    for line in cmd.stdout:
        line = line.decode('UTF-8')
        if "tag title" in line:
            title = line.strip().lstrip('tag title')
            cmus_status[1] = title
            isCmusRunning = 1
        elif "tag artist" in line:
            artist = line.strip().lstrip('tag artist')
        # overwrite artist with albumartist if set
        elif "tag albumartist" in line:
            albumartist = line.strip().lstrip('tag albumartist')
        elif "status playing" in line:
            cmus_status[2] = "\u266B" # pause symbol
            isCmusPlaying = 1
    if albumartist:
        cmus_status[0] = albumartist
    if artist and artist != albumartist:
        if albumartist:
            cmus_status[0] += " ("
        cmus_status[0] += artist
        if albumartist:
            cmus_status[0] += ")"
    trackdata = cmus_status[0] + ": " + cmus_status[1]
    global startDelay
    global spaces
    global scrollCharPosition
    global titlePrevious
    # reinitialisation
    if titlePrevious and title != titlePrevious:
        startDelay = 3
        scrollCharPosition = 0
        spaces = ''
    trackdataDisplay = trackdata + spaces
    maxlength = 30
    # scroll only too long texts
    # wait 3 refresh before scrolling
    if len(trackdata) > maxlength:
        trackdataDisplay = trackdataDisplay[scrollCharPosition:scrollCharPosition + maxlength]
        if isCmusPlaying:
            if startDelay == 0:
                scrollCharPosition += 3
                spaces += '   '
                if len(spaces) >= len(trackdata):
                    scrollCharPosition = 0
                    spaces = ''
            else:
                startDelay -= 1
    if isCmusRunning:
        output = cmus_status[2] + " " + trackdataDisplay
    titlePrevious = title
    return output

def get_volume_global():
    """docstring for get_volume_global"""
    # F026->28, 1F507->09,0A
    output = ''
    color = '#3e3e3e'
    proc = subprocess.Popen('/usr/bin/pamixer --get-volume', shell=True, stdout=subprocess.PIPE)
    volume_stdout = proc.communicate()[0].decode('UTF-8').split('\n')[0]
    percent = int(volume_stdout)
    proc = subprocess.Popen('/usr/bin/pamixer --get-mute', shell=True, stdout=subprocess.PIPE)
    mute_stdout = proc.communicate()[0].decode('UTF-8').split('\n')[0]
    if mute_stdout == 'true':
        output = '\U0001F507 '
    elif percent == 0:
        output = '\U0001F508 '
        color = '#555555'
    elif percent > 0 and percent <= 50:
        output = '\U0001F509 '
        color = '#559900'
    elif percent > 50 and percent <= 75:
        output = '\U0001F50A '
        color = '#995500'
    else:
        output = '\U0001F50A '
        color = '#992200'
    return {'full_text' : '%s' % output + str(percent) + '%', 'name' : 'volume_global', "align" : "center", "color" : color, "separator" : False, "min_width" : 55}

def get_microphone_status():
    # micro: F2A9, F2AA, F2AB
    output = '\uF2A9'
    color = '#555555'
    proc = subprocess.Popen('/usr/bin/pamixer --source alsa_input.hw_0_0 --get-mute', shell=True, stdout=subprocess.PIPE)
    mute_stdout = proc.communicate()[0].decode('UTF-8').split('\n')[0]
    if mute_stdout == 'true':
        output = '\uF2A9'
        color = '#555555'
    else:
        output = '\uF2AB'
        color = '#559900'
    return {'full_text' : '%s' % output, 'name' : 'microphone', 'align' : 'center', 'color' : color, 'separator' : False}

def get_headphone_status():
    """docstring for get_headphone_status"""
    output = "\uF240"
    color = "#555555"
    cmd = subprocess.Popen("cat /proc/asound/card0/codec#2 | grep -i 'Node 0x14' -n -A 12", shell=True, stdout=subprocess.PIPE)
    for line in cmd.stdout:
        line = line.decode('UTF-8')
        if line.find('Pin-ctls: 0x00') > -1:
            color = "#559900"
    #print(output)
    return {'full_text' : '%s' % output, 'name' : 'headphone', "align" : "center", "color" : color, "separator" : False}
    #F240
    # cat /proc/asound/card0/codec#0 | grep -i pin
    # Output when disconnected:
    # Node 0x14 [Pin Complex] wcaps 0x40058d: Stereo Amp-Out
    # Pincap 0x0001003e: IN OUT HP EAPD Detect Trigger
    # Pin Default 0x99130110: [Fixed] Speaker at Int ATAPI
    # Pin-ctls: 0x40: OUT
    # When connected, this last line becomes:
    # Pin-ctls: 0x00

def get_battery_status():
    """docstring for get_battery_status"""
    # battery icons: F211 > F215
    pass

def get_webcam_status():
    """docstring for get_webcam_status"""
    # F2F3, F2F4, F218, F264, F265
    text = "\uF16D"
    color = "#555555"
    if os.path.exists("/dev/video0"):
        color = "#559900"
    return {'full_text' : '%s' % text, 'name' : 'webcam', "align" : "center", "color" : color, "separator" : False, "separator_block_width" : 20}

def get_touchpad_status():
    """docstring for get_touchpad_status"""
    pass

def print_line(message):
    """ Non-buffered printing to stdout. """
    sys.stdout.write(message + '\n')
    sys.stdout.flush()

def read_line():
    """ Interrupted respecting reader for stdin. """
    # try reading a line, removing any extra whitespace
    try:
        line = sys.stdin.readline().strip()
        # i3status sends EOF, or an empty line
        if not line:
            sys.exit(3)
        return line
    # exit on ctrl-c
    except KeyboardInterrupt:
        sys.exit()

if __name__ == '__main__':
    # exit()
    # Skip the first line which contains the version header.
    print_line(read_line())

    # The second line contains the start of the infinite array.
    print_line(read_line())

    while True:
        line, prefix = read_line(), ''
        # ignore comma at start of lines
        if line.startswith(','):
            line, prefix = line[1:], ','

        j = json.loads(line)
        # insert information into the start of the json, but could be anywhere
        # CHANGE THIS LINE TO INSERT SOMETHING ELSE
        #j.insert(0, {'full_text' : '%s' % get_governor(), 'name' : 'gov'})
        j.insert(0, {'full_text' : '%s' % get_linux_version(), 'name': 'linux'})
        j.insert(0, get_webcam_status())
        j.insert(0, get_headphone_status())
        j.insert(0, get_microphone_status())
        j.insert(0, get_volume_global())
        j.insert(0, {
            'full_text' : '%s' % get_cmus_status(),
            'name' : 'cmus',
            'min_width' : 50,
            'align' : 'right',
            'color' : '#aaaae5',
            'separator' : False,
            'separator_block_width' : 15
        })
        # and echo back new encoded json
        print_line(prefix+json.dumps(j))
